Internet Banking Crawler
========================

Pembaca saldo dan mutasi transaksi situs Internet banking. Bank yang sudah
didukung adalah BCA, Bank Mandiri, BNI, Bank Syariah Mandiri, dan Bank Permata.

Syarat umum yang memungkinkan untuk dibuatnya crawler ini adalah ketiadaan
captcha di situs mereka.
