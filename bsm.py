import mechanize
import re
import urllib
from sgmllib import SGMLParser
from datetime import date
from common import (
    BaseBrowser,
    FormParser,
    open_file,
    USER_AGENT_DESKTOP,
    )


def to_float(s):
    return float(s.replace(',', ''))


class SaldoParser(SGMLParser):
    def __init__(self):
        SGMLParser.__init__(self)
        self.catat = False 
        self.hasil = []
        self.baris = []
        self.data = ''
        self.tag = None

    def start_th(self, attrs):
        self.tag = 'th'

    def end_th(self):
        self.tag = None

    def start_td(self, attrs):
        self.tag = 'td'

    def end_td(self):
        if self.catat:
            self.baris.append(self.data)
            self.data = ''

    def start_a(self, attrs):
        if self.catat:
            for attr in attrs:
                if attr[0] == 'href':
                    self.baris.append(attr[1])
                    return

    def end_a(self):
        pass

    def start_tr(self, attrs):
        pass

    def end_tr(self):
        if self.catat:
            if self.baris:
                self.hasil.append(self.baris)
            self.baris = []

    def start_table(self, attrs):
        pass

    def end_table(self):
        self.catat = False

    def handle_data(self, data):
        if self.tag == 'th' and data == 'No. Rekening':
            self.catat = True
        elif self.catat:
            self.data = data.strip()

    def get_clean_data(self):
        r = []
        for no, jenis, mata_uang, rekening, nama, saldo, saldo_efektif,\
                link_trn, link_stm, any_ in self.hasil:
            saldo = to_float(saldo)
            r.append((rekening, jenis, mata_uang, saldo))
        return r


class MutasiParser(SGMLParser):
    def __init__(self):
        SGMLParser.__init__(self)
        self.daftar = []
        self.baris = []
        self.data = []
        self.catat = False
        self.rekening = None
        self.mata_uang = None
        self.saldo = []
        self.selesai = False
        self.pesan_error = ''
        self.catat_rekening = False

    def start_table(self, attrs):
        pass

    def end_table(self):
        self.catat = False 

    def start_tr(self, attrs):
        pass

    def end_tr(self):
        if self.baris:
            if self.baris[4:]:
                tgl = self.baris[1].split()[0]
                kode = self.baris[3]
                ket = self.baris[4]
                debet = self.baris[5]
                kredit = self.baris[6]
                d, m, y = map(lambda x: int(x), tgl.split('-'))
                tgl = date(y, m, d) 
                row = [tgl, kode, ket, debet, kredit]
                self.daftar.append(row)
            self.baris = []

    def start_td(self, attrs):
        pass

    def end_td(self):
        if self.data:
            self.baris.append(' '.join(self.data).strip())
            self.data = []

    def end_font(self):
        if self.pesan_error:
            self.selesai = True

    def handle_data(self, data):
        if self.selesai:
            return
        if data == 'Kredit':
            self.catat = True 
            return
        if data.find('Error') > -1:
            self.pesan_error += str(data)
            return
        match = re.compile('Rekening:').search(data)
        if match:
            self.catat_rekening = True
            return
        if data.strip() and self.catat_rekening:
            t = data.split()
            self.rekening = t[1]
            self.mata_uang = t[0]
            self.catat_rekening = False
            return
        match = re.compile('Saldo per (.*) : (.*)').search(data)
        if match:
            self.saldo.append([match.group(1), match.group(2).replace(',','')])
            return
        if not self.catat:
            return
        self.data.append(data.strip())

    def get_clean_data(self):
        r = []
        for tgl, kode, ket, debet, kredit in self.daftar:
            if debet:
                nominal = - to_float(debet)
            else:
                nominal = to_float(kredit)
            r.append((self.rekening, tgl, ket, nominal, 0))
        return r 


class Browser(BaseBrowser):
    def __init__(self, username, password, parser, output_file=None):
        BaseBrowser.__init__(self, 'https://bsmnet.syariahmandiri.co.id',
            username, password, parser, output_file=output_file)
        cookies = mechanize.CookieJar()
        opener = mechanize.build_opener(mechanize.HTTPCookieProcessor(cookies))
        opener.addheaders = [('User-Agent', USER_AGENT_DESKTOP)]
        mechanize.install_opener(opener)

    def login(self):
        self.open_url()
        resp = self.open_url('/cms/index.php')
        parser = FormParser()
        parser.feed(resp.read())
        p = dict(parser.inputs)
        p['LOGNAME'] = self.username 
        p['PASSWORD'] = self.password
        parser = MutasiParser()
        self.info('Login ' + self.username)
        content = self.get_content('/cms/index.php', p)
        parser.feed(content)
        if parser.pesan_error:
            self.error(parser.pesan_error)
        else:
            return True

    def logout(self):
        p = {'cmd': 'CMD_LOGOUT'}
        self.open_url('/cms/index.php', GET_data=p)

    def open_url(self, url=None, POST_data=None, GET_data={}):
        url = self.get_url(url, GET_data)
        if POST_data is not None:
            data = urllib.urlencode(POST_data)
            req = mechanize.Request(self.base_url + '/cms/index.php', data)
            self.info('POST %s' % url)
            return mechanize.urlopen(req)
        self.info('GET %s' % url)
        return mechanize.urlopen(url)


class SaldoBrowser(Browser):
    def __init__(self, username, password, output_file=None):
        Browser.__init__(self, username, password, SaldoParser, output_file)

    def browse(self):
        p = dict(cmd='CMD_REK_TAB')
        return self.open_url('/cms/index.php', GET_data=p)
        
 
class MutasiBrowser(Browser):
    def __init__(self, username, password, option_file=None):
        Browser.__init__(self, username, password, MutasiParser, option_file)

    def browse(self, tgl):
        content = self.get_content('/cms/index.php?cmd=CMD_REK_TAB')
        parser = SaldoParser()
        parser.feed(content)
        url = '/cms/' + parser.hasil[0][7]
        content = self.get_content(url)
        parser = FormParser()
        parser.feed(content)
        p = parser.inputs
        p['DATA_DOWNLOAD'] = ''
        p['DATE_FROM_DD'] = p['DATE_UNTIL_DD'] = str(tgl.day).zfill(2)
        p['DATE_FROM_MM'] = p['DATE_UNTIL_MM'] = str(tgl.month).zfill(2)
        p['DATE_FROM_YY'] = p['DATE_UNTIL_YY'] = str(tgl.year)
        return self.open_url('/cms/index.php', p)


if __name__ == '__main__':
    import sys
    from optparse import OptionParser
    from pprint import pprint
    from common import to_date
    pars = OptionParser()
    pars.add_option('-u', '--username')
    pars.add_option('-p', '--password')
    pars.add_option('-d', '--date', help='dd-mm-yyyy')
    pars.add_option('', '--mutasi-file')
    pars.add_option('', '--saldo-file')
    pars.add_option('', '--output-file')
    option, remain = pars.parse_args(sys.argv[1:])

    if option.mutasi_file:
        content = open_file(option.mutasi_file)
        parser = MutasiParser()
        parser.feed(content)
        pprint(parser.get_clean_data())
        sys.exit()

    if option.saldo_file:
        content = open_file(option.saldo_file)
        parser = SaldoParser()
        parser.feed(content)
        pprint(parser.get_clean_data())
        sys.exit()

    if not option.username or not option.password:
        print('--username dan --password harus diisi')
        sys.exit()

    if option.date:
        crawler = MutasiBrowser(option.username, option.password,
                                option.output_file)
        tgl = to_date(option.date)
        data = crawler.run(tgl)
        pprint(data)
    else:
        crawler = SaldoBrowser(option.username, option.password,
                               option.output_file)
        data = crawler.run()
        pprint(data)
